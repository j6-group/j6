package org.j8.samples;

import org.j8.sdk.J8TestSuite;
import org.j8.sdk.TestCase;
import org.j8.sdk.TestSuite;

import java.util.List;

@J8TestSuite("hello-world")
public class HelloWorldTestSuite implements TestSuite {
    @Override
    public List<Class<? extends TestCase>> getTestCases() {
        return List.of(
            HelloWorldTest.class
        );
    }
}
