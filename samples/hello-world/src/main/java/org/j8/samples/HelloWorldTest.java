package org.j8.samples;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.j8.sdk.J8Test;
import org.j8.sdk.TestCase;
import org.j8.sdk.TestExecutionToolchain;
import org.j8.sdk.http.validation.HttpResponseValidator;
import org.j8.sdk.http.J8HttpClient;

import static org.j8.sdk.http.J8HttpRequest.*;

@J8Test
@RequiredArgsConstructor
public class HelloWorldTest implements TestCase {

    private final J8HttpClient httpClient;

    private final HttpResponseValidator httpResponseValidator;

    public HelloWorldTest(TestExecutionToolchain toolchain) {
        this.httpClient = toolchain.httpProtocol().getClient();
        this.httpResponseValidator = toolchain.httpProtocol().getResponseValidator();
    }

    @Override
    public String getName() {
        return "hello-world";
    }

    @Override
    @SneakyThrows
    public void run() {
        System.out.println("Hello World!");

        final var createOrderResponse = httpClient.execute(
            get("https://open-weather13.p.rapidapi.com/city/landon/EN").build()
        ).get();

        httpClient.execute(post("https://google.com")
            .json("{\"foo\": \"bar\"}")
            .build());
    }
}
