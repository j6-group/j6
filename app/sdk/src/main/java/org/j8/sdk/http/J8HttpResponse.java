package org.j8.sdk.http;

import lombok.Value;

import java.util.List;
import java.util.Map;

@Value
public class J8HttpResponse<RT> {

    int statusCode;

    String statusMessage;

    Map<String, List<String>> headers;

    RT body;

}
