package org.j8.sdk.http.validation;

import lombok.RequiredArgsConstructor;
import org.j8.sdk.ValidationFailedException;
import org.j8.sdk.http.J8HttpResponse;

@RequiredArgsConstructor
public class StatusValidator<T> {

    private final J8HttpResponse<T> response;

    void isOk() {
        if (response.getStatusCode() != 200) {
            throw new ValidationFailedException();
        }
    }

    void is(int statusCode) {
        if (statusCode != response.getStatusCode()) {
            throw new ValidationFailedException();
        }
    }

}
