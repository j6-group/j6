package org.j8.sdk;

public interface TestCase {

    String getName();

    default void runPreconditions() {
        /* _ */
    }

    void run();

}
