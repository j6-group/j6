package org.j8.sdk.http;

import java.util.concurrent.CompletableFuture;

public interface J8HttpClient {

    <RT> CompletableFuture<J8HttpResponse<RT>> execute(J8HttpRequest request, Class<RT> responseClass);

    <RT> CompletableFuture<J8HttpResponse<Void>> execute(J8HttpRequest request);

}
