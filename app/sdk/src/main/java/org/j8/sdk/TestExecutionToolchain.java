package org.j8.sdk;

import org.j8.sdk.http.HttpProtocol;

public interface TestExecutionToolchain {

    HttpProtocol httpProtocol();

}
