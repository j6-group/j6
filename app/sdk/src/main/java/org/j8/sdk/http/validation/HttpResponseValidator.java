package org.j8.sdk.http.validation;

import org.j8.sdk.http.J8HttpResponse;

public class HttpResponseValidator {

    public static <T> void check(J8HttpResponse<T> response);

}
