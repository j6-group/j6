package org.j8.sdk;

import java.util.List;

public interface TestSuite {

    List<Class<? extends TestCase>> getTestCases();

}
