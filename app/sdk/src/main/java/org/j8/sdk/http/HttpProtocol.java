package org.j8.sdk.http;

import lombok.Value;
import org.j8.sdk.http.validation.HttpResponseValidator;

@Value
public class HttpProtocol {

    J8HttpClient client;

    HttpResponseValidator responseValidator;

}
