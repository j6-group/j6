package org.j8.sdk.http;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.Value;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.*;

@Value
public class J8HttpRequest {

    String url;

    J8HttpMethod method;

    Map<String, List<String>> headers;

    Map<String, List<String>> query;

    String body;

    public boolean hasBody() {
        return body != null && !body.isEmpty();
    }

    public static GetBuilder get(String url) {
        return new GetBuilder(url);
    }

    public static PostBuilder post(String url) {
        return new PostBuilder(url);
    }

    public static PutBuilder put(String url) {
        return new PutBuilder(url);
    }

    public static DeleteBuilder delete(String url) {
        return new DeleteBuilder(url);
    }

    @Getter
    @RequiredArgsConstructor
    public static class GetBuilder {

        private final String url;

        private final J8HttpMethod method;

        private final Map<String, List<String>> headers = new HashMap<>();

        private final Map<String, List<String>> query = new HashMap<>();

        public GetBuilder(String url) {
            this.url = url;
            this.method = J8HttpMethod.GET;
        }

        public GetBuilder header(String key, String value) {
            headers.put(key, new ArrayList<>());
            headers.get(key).add(value);
            return this;
        }

        public GetBuilder query(String key, String value) {
            query.put(key, new ArrayList<>());
            query.get(key).add(value);
            return this;
        }

        public J8HttpRequest build() {
            return new J8HttpRequest(
                getUrl(),
                getMethod(),
                getHeaders(),
                getQuery(),
                null
            );
        }

    }

    @Getter
    @RequiredArgsConstructor
    public static class DeleteBuilder {

        private final String url;

        private final J8HttpMethod method;

        private final Map<String, List<String>> headers = new HashMap<>();

        private final Map<String, List<String>> query = new HashMap<>();

        public DeleteBuilder(String url) {
            this.url = url;
            this.method = J8HttpMethod.DELETE;
        }

        public DeleteBuilder header(String key, String value) {
            headers.put(key, new ArrayList<>());
            headers.get(key).add(value);
            return this;
        }

        public DeleteBuilder query(String key, String value) {
            query.put(key, new ArrayList<>());
            query.get(key).add(value);
            return this;
        }

        public J8HttpRequest build() {
            return new J8HttpRequest(
                getUrl(),
                getMethod(),
                getHeaders(),
                getQuery(),
                null
            );
        }

    }

    @Getter
    @RequiredArgsConstructor
    public static class PostBuilder {

        private final String url;

        private final J8HttpMethod method;

        private final Map<String, List<String>> headers = new HashMap<>();

        private final Map<String, List<String>> query = new HashMap<>();

        private String body;

        public PostBuilder(String url) {
            this.url = url;
            this.method = J8HttpMethod.POST;
        }

        public PostBuilder header(String key, String value) {
            headers.put(key, new ArrayList<>());
            headers.get(key).add(value);
            return this;
        }

        public PostBuilder query(String key, String value) {
            query.put(key, new ArrayList<>());
            query.get(key).add(value);
            return this;
        }

        public PostBuilder json(String plainValue) {
            header("Content-Type", "application/json");
            body = plainValue;
            return this;
        }

        @SneakyThrows
        public PostBuilder json(InputStream inputStream) {
            header("Content-Type", "application/json");
            body = new String(inputStream.readAllBytes(), StandardCharsets.UTF_8);
            return this;
        }

        public J8HttpRequest build() {
            return new J8HttpRequest(
                getUrl(),
                getMethod(),
                getHeaders(),
                getQuery(),
                body
            );
        }

    }

    @Getter
    @RequiredArgsConstructor
    public static class PutBuilder {

        private final String url;

        private final J8HttpMethod method;

        private final Map<String, List<String>> headers = new HashMap<>();

        private final Map<String, List<String>> query = new HashMap<>();

        private String body;

        public PutBuilder(String url) {
            this.url = url;
            this.method = J8HttpMethod.PUT;
        }

        public PutBuilder header(String key, String value) {
            headers.put(key, new ArrayList<>());
            headers.get(key).add(value);
            return this;
        }

        public PutBuilder query(String key, String value) {
            query.put(key, new ArrayList<>());
            query.get(key).add(value);
            return this;
        }

        public PutBuilder json(String plainValue) {
            header("Content-Type", "application/json");
            body = plainValue;
            return this;
        }

        @SneakyThrows
        public PutBuilder json(InputStream inputStream) {
            header("Content-Type", "application/json");
            body = new String(inputStream.readAllBytes(), StandardCharsets.UTF_8);
            return this;
        }

        public J8HttpRequest build() {
            return new J8HttpRequest(
                getUrl(),
                getMethod(),
                getHeaders(),
                getQuery(),
                body
            );
        }

    }

}
