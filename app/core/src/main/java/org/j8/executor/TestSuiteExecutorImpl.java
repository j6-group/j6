package org.j8.executor;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.j8.configuration.RunConfiguration;
import org.j8.sdk.TestCase;
import org.j8.sdk.TestSuite;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Constructor;

@Slf4j
public class TestSuiteExecutorImpl implements TestSuiteExecutor {

    @Override
    public void execute(Class<? extends TestSuite> testSuite, RunConfiguration configuration) {
        final var testSuiteInstance = createTestSuiteInstance(testSuite, configuration);
        testSuiteInstance.getTestCases().forEach(item -> executeTest(createTestCaseInstance(item, configuration)));
    }

    private void executeTest(TestCase testCase) {
        testCase.runPreconditions();
        testCase.run();
    }

    @SneakyThrows
    private @NotNull TestSuite createTestSuiteInstance(Class<? extends TestSuite> testSuite,
                                                              RunConfiguration configuration) {
         return testSuite.getDeclaredConstructor(null).newInstance();
    }

    @SneakyThrows
    private @NotNull TestCase createTestCaseInstance(Class<? extends TestCase> testSuite,
                                                              RunConfiguration configuration) {
         return testSuite.getDeclaredConstructor(null).newInstance();
    }

    private <T> Constructor<T> getConstructor(Class<T> clazz, Class<?>... parameterTypes) {
        try {
            return clazz.getDeclaredConstructor(parameterTypes);
        } catch (final NoSuchMethodException e) {
            return null;
        }
    }
}
