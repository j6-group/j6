package org.j8.executor;

import org.j8.configuration.RunConfiguration;
import org.j8.sdk.TestSuite;

public interface TestSuiteExecutor {

    void execute(Class<? extends TestSuite> testSuite, RunConfiguration configuration);

}
