package org.j8.configuration;

public final class ArgsRunConfigurationFactory implements RunConfigurationFactory {

    @Override
    public RunConfiguration createRunConfiguration(String[] args) {
        final var configurationBuilder = RunConfiguration.builder();
        for (int i = 0; i < args.length; i++) {
            switch (args[i]) {

                case "--help": {
                    configurationBuilder.displayHelp(true);
                    return configurationBuilder.build();
                }

                case "--test": case "-t": {
                    configurationBuilder.testClass(args[++i]);
                    break;
                }

                case "-v": {
                    configurationBuilder.logLevel(1);
                    break;
                }

                case "-vv": {
                    configurationBuilder.logLevel(2);
                    break;
                }

                case "-vvv": {
                    configurationBuilder.logLevel(3);
                    break;
                }

                case "-ts": {
                    configurationBuilder.testSuite(args[++i]);
                    break;
                }

                case "--silent": {
                    configurationBuilder.logLevel(-99);
                    break;
                }

                default: {
                    throw new IllegalArgumentException("Unknown option: " + args[i]);
                }
            }
        }

        return configurationBuilder.build();
    }

}
