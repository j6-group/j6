package org.j8.configuration;

public interface RunConfigurationFactory {
    RunConfiguration createRunConfiguration(String[] args);
}
