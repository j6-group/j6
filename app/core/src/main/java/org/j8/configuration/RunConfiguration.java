package org.j8.configuration;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class RunConfiguration {

    boolean displayHelp;

    String testClass;

    String testSuite;

    int logLevel;

    public boolean isLogsAllowed() {
        return logLevel >= 0;
    }

}
