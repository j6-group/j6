package org.j8.toolchain.http;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import okhttp3.Response;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

@RequiredArgsConstructor
public class JsonHttpResponseTransformer implements HttpResponseTransformer {
    private final ObjectMapper objectMapper;

    @Override
    public boolean isAssumableTo(Response response) {
        return Optional.ofNullable(response.header("Content-Type"))
            .map(header -> header.contains("application/json"))
            .orElse(false);
    }

    @Override
    public <T> T transform(Response response, Class<T> clazz) throws IOException {
        if (response.body() == null) {
            return null;
        }
        return objectMapper.readValue(new String(response.body().bytes(), StandardCharsets.UTF_8), clazz);
    }
}
