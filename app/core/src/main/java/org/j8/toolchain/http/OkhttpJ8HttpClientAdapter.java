package org.j8.toolchain.http;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.j8.sdk.http.J8HttpClient;
import org.j8.sdk.http.J8HttpRequest;
import org.j8.sdk.http.J8HttpResponse;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@RequiredArgsConstructor
@Slf4j
public class OkhttpJ8HttpClientAdapter implements J8HttpClient {

    private final OkHttpClient okHttpClient;

    private final List<HttpResponseTransformer> responseTransformers = List.of(
        new JsonHttpResponseTransformer(new ObjectMapper()),
        new StringHttpResponseTransformer()
    );

    @Override
    public <RT> CompletableFuture<J8HttpResponse<Void>> execute(J8HttpRequest request) {
        return execute(request, Void.class);
    }

    @Override
    public <RT> CompletableFuture<J8HttpResponse<RT>> execute(J8HttpRequest request, Class<RT> responseClass) {
        final var okhttpRequest = new Request.Builder();

        request.getHeaders()
            .forEach((key, values) -> values.forEach(headerValue ->
                okhttpRequest.header(key, headerValue)
            ));

        if (request.hasBody()) {
            okhttpRequest.setBody$okhttp(
                RequestBody.create(
                    request.getBody().getBytes(StandardCharsets.UTF_8)
                )
            );
        }

        final var future = new CompletableFuture<J8HttpResponse<RT>>();
        okHttpClient.newCall(okhttpRequest.build())
            .enqueue(new Callback() {
                @Override
                public void onFailure(@NotNull Call call, @NotNull IOException e) {
                    future.completeExceptionally(e);
                }

                @Override
                public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                    future.complete(toJ8Response(response, responseClass));
                }
            });
        return future;
    }

    private <RT> J8HttpResponse<RT> toJ8Response(Response response, Class<RT> responseClass) throws IOException {
        if (Void.class.equals(responseClass)) {
            return new J8HttpResponse<>(
                response.code(),
                response.message(),
                response.headers().toMultimap(),
                null
            );
        }

        final var assumableResponseTransformer = responseTransformers.stream()
            .filter(transformer -> transformer.isAssumableTo(response))
            .findAny()
            .orElseThrow(() -> new IOException("No transformer found for response"));

        return new J8HttpResponse<>(
            response.code(),
            response.message(),
            response.headers().toMultimap(),
            assumableResponseTransformer.transform(response, responseClass)
        );
    }
}
