package org.j8.toolchain.http;

import okhttp3.Response;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class StringHttpResponseTransformer implements HttpResponseTransformer {
    @Override
    public boolean isAssumableTo(Response response) {
        return true;
    }

    @Override
    public <T> T transform(Response response, Class<T> clazz) throws IOException {
        if (!String.class.isAssignableFrom(clazz)) {
            throw new IOException("Requested type is not a String");
        }
        if (response.body() == null) {
            return null;
        }
        return (T) new String(response.body().bytes(), StandardCharsets.UTF_8);
    }
}
