package org.j8.toolchain.http;

import okhttp3.Response;

import java.io.IOException;

public interface HttpResponseTransformer {

    boolean isAssumableTo(Response response);

    <T> T transform(Response response, Class<T> clazz) throws IOException;

}
