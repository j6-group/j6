package org.j8.loader;

import lombok.Builder;
import lombok.SneakyThrows;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.j8.configuration.RunConfiguration;
import org.j8.sdk.J8Test;
import org.j8.sdk.J8TestSuite;
import org.j8.sdk.TestCase;
import org.j8.sdk.TestSuite;

import java.lang.reflect.Modifier;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;
import java.util.jar.JarFile;

@Slf4j
public class JarFileTestLoader implements TestLoader {

    private final String fileName;

    public JarFileTestLoader(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public Class<? extends TestSuite> loadTestSuite(String testSuite, RunConfiguration configuration) {
        final var importResult = importTestsFromJar(configuration);
        if (testSuite == null) {
            if (importResult.testSuites.size() == 1) {
                return importResult.testSuites.entrySet()
                    .iterator()
                    .next()
                    .getValue();
            }
            if (configuration.getLogLevel() > 1) {

            }
            throw new IllegalArgumentException("More than one test suite found. You must specify which one to run.");
        }
        return Optional.ofNullable(importResult.testSuites.get(testSuite))
            .orElseThrow(() -> new IllegalArgumentException("Test suite with name '" + testSuite + "' not found."));
    }

    @SneakyThrows
    private JarLoadingResult importTestsFromJar(RunConfiguration configuration) {
        final var testCaseList = new ArrayList<Class<? extends TestCase>>();
        final var testSuites = new HashMap<String, Class<? extends TestSuite>>();

        if (configuration.getLogLevel() > 1) {
            log.info("Loading tests from file: {}", fileName);
        }

        try (final var jarFile = new JarFile(fileName)) {
            final var entries = jarFile.entries();
            final var urls = new URL[]{ URI.create("jar:file:" + fileName + "!/").toURL() };
            try (final var classLoader = URLClassLoader.newInstance(urls)) {
                while (entries.hasMoreElements()) {
                    final var entry = entries.nextElement();

                    if (configuration.getLogLevel() > 3) {
                        log.info("Entry found: {}", entry.getName());
                    }

                    if(entry.isDirectory() || !entry.getName().endsWith(".class")){
                        continue;
                    }
                    // -6 because of .class
                    final var className = entry
                        .getName()
                        .substring(0, entry.getName().length() - 6)
                        .replace('/', '.');

                    if (configuration.getLogLevel() > 3) {
                        log.info("Class found: {}", className);
                    }

                    final var clazz = classLoader.loadClass(className);
                    if (isValidTestCase(clazz)) {
                        if (configuration.getLogLevel() > 1) {
                            log.info("Test case found: {}", clazz.getName());
                        }
                        testCaseList.add((Class<? extends TestCase>) clazz);
                    } else if (configuration.getLogLevel() > 1) {
                        log.info("Class {} is not a valid test case", className);
                    }

                    if (isValidTestSuite(clazz)) {
                        if (configuration.getLogLevel() > 1) {
                            log.info("Test suite found: {}", clazz.getName());
                        }
                        testSuites.put(
                            clazz.getAnnotation(J8TestSuite.class).value(),
                            (Class<? extends TestSuite>) clazz
                        );
                    } if (configuration.getLogLevel() > 1) {
                        log.info("Class {} is not a valid test suite", className);
                    }
                }
            }
        }
        return new JarLoadingResult(testCaseList, testSuites);
    }

    private static boolean isValidTestCase(Class<?> clazz) {
        return TestCase.class.isAssignableFrom(clazz)
            && !Modifier.isAbstract(clazz.getModifiers())
            && !Modifier.isInterface(clazz.getModifiers())
            && clazz.isAnnotationPresent(J8Test.class);
    }

    private static boolean isValidTestSuite(Class<?> clazz) {
        return TestSuite.class.isAssignableFrom(clazz)
            && !Modifier.isAbstract(clazz.getModifiers())
            && !Modifier.isInterface(clazz.getModifiers())
            && clazz.isAnnotationPresent(J8TestSuite.class);
    }

    @Value
    @Builder
    private static class JarLoadingResult {

        List<Class<? extends TestCase>> testCases;

        Map<String, Class<? extends TestSuite>> testSuites;

    }

}
