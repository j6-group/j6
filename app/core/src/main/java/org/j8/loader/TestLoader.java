package org.j8.loader;

import org.j8.configuration.RunConfiguration;
import org.j8.sdk.TestSuite;

public interface TestLoader {

    Class<? extends TestSuite> loadTestSuite(String testSuite, RunConfiguration configuration);

}
