package org.j8;

import lombok.extern.slf4j.Slf4j;
import org.j8.configuration.ArgsRunConfigurationFactory;
import org.j8.configuration.RunConfigurationFactory;
import org.j8.loader.JarFileTestLoader;

@Slf4j
public class Application {

    private final static RunConfigurationFactory runConfigurationFactory = new ArgsRunConfigurationFactory();

    public static void main(String[] args) {
        final var configuration = runConfigurationFactory.createRunConfiguration(args);
        log.info("Run configuration: {}", configuration);

        if (configuration.isDisplayHelp()) {
            displayHelp();
            return;
        }

        if (configuration.getTestClass() == null) {
            throw new IllegalStateException("Test class is required");
        }

        final var testLoader = new JarFileTestLoader(configuration.getTestClass());
        final var testSuiteToRun = testLoader.loadTestSuite(configuration.getTestSuite(), configuration);

        if (configuration.isLogsAllowed()) {
            log.info("Test suite to run: {}", testSuiteToRun);
        }
    }

    private static void displayHelp() {
        System.out.println("Usage: java -jar application.jar");
    }
}
